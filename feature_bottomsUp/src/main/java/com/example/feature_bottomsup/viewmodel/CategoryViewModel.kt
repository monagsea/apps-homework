package com.example.feature_bottomsup.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_bottomsup.model.BottomsUpRepo
import com.example.feature_bottomsup.model.response.CategoryDTO
import com.example.feature_bottomsup.view.category.CategoryState
import kotlinx.coroutines.launch

class CategoryViewModel : ViewModel() {
    private val repo by lazy { BottomsUpRepo }

    private val _state = MutableLiveData(CategoryState())
    val state: LiveData<CategoryState> get() = _state

    private val _stringList = MutableLiveData<CategoryDTO>()
    val stringList: LiveData<CategoryDTO> get() = _stringList

    init {
        viewModelScope.launch {
            val categoryDTO = repo.getCategories()
            _state.value = CategoryState(categories = categoryDTO.categoryItems)
        }
    }

    fun getCategories() {
        viewModelScope.launch {
            val category = repo.getCategories()
            _stringList.value = category
        }
    }
}