package com.example.feature_bottomsup.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_bottomsup.model.BottomsUpRepo
import com.example.feature_bottomsup.model.response.DrinkDetailDTO
import com.example.feature_bottomsup.view.drinkDetails.DetailState
import kotlinx.coroutines.launch

class DrinkDetailsViewModel : ViewModel() {
    private val repo by lazy { BottomsUpRepo }

    private val _state = MutableLiveData(DetailState())
    val state: LiveData<DetailState> get() = _state

    private val _stateList = MutableLiveData<DrinkDetailDTO>()
    val stateList: LiveData<DrinkDetailDTO> get() = _stateList


    fun getDetails(drinkId: String) {
        viewModelScope.launch {
            val drinkDetailsDTO = repo.getDrinkDetails(drinkId)
            _state.value = DetailState(details = drinkDetailsDTO.drinks)
            _stateList.value = drinkDetailsDTO
        }
    }
}