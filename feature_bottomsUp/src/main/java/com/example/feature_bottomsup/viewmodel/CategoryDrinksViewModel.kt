package com.example.feature_bottomsup.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_bottomsup.model.BottomsUpRepo
import com.example.feature_bottomsup.model.response.CategoryDrinksDTO
import com.example.feature_bottomsup.view.drinks.CategoryDrinksState
import kotlinx.coroutines.launch

class CategoryDrinksViewModel() : ViewModel() {

    private val repo by lazy { BottomsUpRepo }

//    Using Jetpack.Components Lifecycle - first import dependency
//    private val state = liveData {
//        emit(CategoryDrinksState)
//        viewModelScope.launch {
//            val categoryDrinks = repo.getCategoryDrinks(category)
//            emit(CategoryDrinksState(categoryDrinks = categoryDrinks.drinks))
//        }
//    }

    private val _state = MutableLiveData<CategoryDrinksState>()
    val state: LiveData<CategoryDrinksState> get() = _state

    private val _stateList = MutableLiveData<CategoryDrinksDTO>()
    val stateList: LiveData<CategoryDrinksDTO> get() = _stateList

    fun getDrinks(category: String) {
        viewModelScope.launch {
            val categoryDrinksDTO = repo.getCategoryDrinks(category)
            _state.value = CategoryDrinksState(categories = categoryDrinksDTO.drinks)
            _stateList.value = categoryDrinksDTO
        }
    }
}