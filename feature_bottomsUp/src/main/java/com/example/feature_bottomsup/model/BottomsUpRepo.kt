package com.example.feature_bottomsup.model

import com.example.feature_bottomsup.model.remote.BottomsUpService
import com.example.feature_bottomsup.model.response.CategoryDTO
import com.example.feature_bottomsup.model.response.CategoryDrinksDTO
import com.example.feature_bottomsup.model.response.DrinkDetailDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object BottomsUpRepo {

    private val bottomsUpService: BottomsUpService by lazy { BottomsUpService.getInstance() }

    suspend fun getCategories(): CategoryDTO = withContext(Dispatchers.IO) {
        bottomsUpService.getCategories()
    }

    suspend fun getCategoryDrinks(category: String): CategoryDrinksDTO =
        withContext(Dispatchers.IO) {
            bottomsUpService.getCategoryDrinks(category)
        }

    suspend fun getDrinkDetails(drinkId: String): DrinkDetailDTO = withContext(Dispatchers.IO) {
        bottomsUpService.getDrinkDetails(drinkId)
    }
}