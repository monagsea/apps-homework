package com.example.feature_bottomsup.view.category

import com.example.feature_bottomsup.model.response.CategoryDTO


data class CategoryState(
    val categories: List<CategoryDTO.CategoryItem> = emptyList()
)