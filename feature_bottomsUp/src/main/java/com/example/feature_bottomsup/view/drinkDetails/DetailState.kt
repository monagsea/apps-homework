package com.example.feature_bottomsup.view.drinkDetails

import com.example.feature_bottomsup.model.response.DrinkDetailDTO

data class DetailState(
    val details: List<DrinkDetailDTO.Drink> = emptyList()
)