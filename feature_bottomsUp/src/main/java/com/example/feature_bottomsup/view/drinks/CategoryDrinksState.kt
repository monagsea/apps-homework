package com.example.feature_bottomsup.view.drinks

import com.example.feature_bottomsup.model.response.CategoryDrinksDTO

data class CategoryDrinksState(
    val categories: List<CategoryDrinksDTO.Drink> = emptyList()
)
