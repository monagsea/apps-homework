package com.example.feature_bottomsup.view.drinkDetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.feature_bottomsup.adapter.DrinkDetailsAdapter
import com.example.feature_bottomsup.databinding.FragmentDetailBinding
import com.example.feature_bottomsup.viewmodel.DrinkDetailsViewModel

class DetailFragment : Fragment() {
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val drinkDetailsViewModel by viewModels<DrinkDetailsViewModel>()
    private val drinkDetailsAdapter by lazy { DrinkDetailsAdapter() }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDetailBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val drinkId = arguments?.getString("drinkId")

        fun displayCategories() =
            with(binding.rvList) {
                layoutManager =
                    LinearLayoutManager(this.context)
                adapter = DrinkDetailsAdapter().apply {
                    with(drinkDetailsViewModel) {
                        if (drinkId != null) {
                            getDetails(drinkId)
                        }
                        stateList.observe(viewLifecycleOwner) {
                            addDetails(it.drinks)
                        }
                    }
                }
            }
        displayCategories()
    }
}