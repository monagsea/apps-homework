package com.example.feature_shibe.view.shibelist

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_shibe.databinding.ItemShibeBinding
import com.example.feature_shibe.model.ShibeRepo
import com.example.feature_shibe.model.local.entity.Shibe
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShibeAdapter(
    private val shibes: List<Shibe>, private val repo: ShibeRepo
) : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = ShibeViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        val url = shibes[position]
        holder.loadShibeImage(url, repo)
    }

    override fun getItemCount() = shibes.size

    class ShibeViewHolder(
        private val binding: ItemShibeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadShibeImage(shibe: Shibe, repo: ShibeRepo) {
            with(binding) {
                Picasso.get().load(shibe.url).into(ivShibe)

                val color = if (shibe.favorite) Color.MAGENTA else Color.WHITE
                binding.favoriteIcon.setColorFilter(color)

                binding.ivShibe.setOnClickListener() {
                    shibe.favorite = !shibe.favorite
                    CoroutineScope(Dispatchers.IO).launch { repo.shibeDao.update(shibe) }
                    val color = if (shibe.favorite) Color.MAGENTA else Color.WHITE
                    binding.favoriteIcon.setColorFilter(color)
                }
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemShibeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> ShibeViewHolder(binding) }
        }
    }
}
